package cz.gopas.calc

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ConstantDao {
    @Query("SELECT * FROM ConstantEntity")
    fun getAll(): LiveData<List<ConstantEntity>>

    @Insert
    fun insert(vararg constans: ConstantEntity)
}