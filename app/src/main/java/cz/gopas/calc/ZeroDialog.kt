package cz.gopas.calc

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

class ZeroDialog : DialogFragment() {
    init {
//        isCancelable = false
    }

    override fun onCreateDialog(savedInstanceState: Bundle?) = AlertDialog.Builder(requireContext())
        .setMessage(R.string.zero)
        .create()
}