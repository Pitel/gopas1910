package cz.gopas.calc

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_constant.*

class ConstantFragment : Fragment(R.layout.fragment_constant) {
    private companion object {
        private val TAG = ConstantFragment::class.java.simpleName
    }

    private val viewModel by lazy { ViewModelProviders.of(this)[ConstantViewModel::class.java] }
    private val sharedViewModel by lazy { ViewModelProviders.of(requireActivity())[ConstantSharedViewModel::class.java] }

    /*
    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel.constants.observe(this, Observer { Log.v(TAG, it.toString()) })
    }
     */

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = ConstantAdapter {
            sharedViewModel.constant.value = it
            requireActivity().onBackPressed()
        }
        with(recycler_view) {
            this.adapter = adapter
            setHasFixedSize(true)
        }
        viewModel.constants.observe(viewLifecycleOwner, Observer { constants ->
            adapter.submitList(
                constants + List(1000) { index ->
                    ConstantEntity(
                        index.toString(),
                        index.toFloat()
                    )
                }
            )
        })
    }
}