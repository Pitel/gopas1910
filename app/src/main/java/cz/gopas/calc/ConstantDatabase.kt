package cz.gopas.calc

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(ConstantEntity::class), version = 1)
abstract class ConstantDatabase : RoomDatabase() {
    abstract fun constantDao(): ConstantDao
}