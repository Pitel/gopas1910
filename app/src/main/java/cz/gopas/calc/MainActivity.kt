package cz.gopas.calc

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit

class MainActivity : AppCompatActivity() {
	private companion object {
		private val TAG = MainActivity::class.java.simpleName
		init {
			FragmentManager.enableDebugLogging(BuildConfig.DEBUG)
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		if (savedInstanceState == null) {
			supportFragmentManager.commit {
				add(android.R.id.content, CalcFragment())
			}
		}

		Toast.makeText(this, "Create", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "Create")

		try {
			Log.i(TAG, intent.dataString)
		} catch(t: Throwable) {
			Log.w(TAG, t)
		}
	}

	/*
	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
		outState.putString("result", result.text.toString())
	}

	override fun onRestoreInstanceState(savedInstanceState: Bundle) {
		super.onRestoreInstanceState(savedInstanceState)
		result.text = savedInstanceState.getString("result", "")
	}
	 */

	override fun onStart() {
		super.onStart()
		Toast.makeText(this, "Start", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "Start")
	}

	override fun onResume() {
		super.onResume()
		Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "Resume")
	}

	override fun onPause() {
		Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "Pause")
		super.onPause()
	}

	override fun onStop() {
		Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "Stop")
		super.onStop()
	}

	override fun onDestroy() {
		Toast.makeText(this, "Destroy", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "Destroy")
		super.onDestroy()
	}
}
