package cz.gopas.calc

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ConstantEntity(
    @PrimaryKey
    val name: String,
    val value: Float
)