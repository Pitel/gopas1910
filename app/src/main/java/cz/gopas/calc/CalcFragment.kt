package cz.gopas.calc

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_calc.*

class CalcFragment : Fragment(R.layout.fragment_calc) {
    private companion object {
        private val TAG = CalcFragment::class.java.simpleName
    }

    init {
        setHasOptionsMenu(true)
    }

    private val viewModel by lazy { ViewModelProviders.of(this)[CalcViewModel::class.java] }
    private val sharedViewModel by lazy { ViewModelProviders.of(requireActivity())[ConstantSharedViewModel::class.java] }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.result.observe(viewLifecycleOwner, Observer { res -> result.text = res })
        sharedViewModel.constant.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                a.setText(it.value.toString())
                sharedViewModel.constant.value = null
            }
        })

        calc.setOnClickListener {
            Log.d(TAG, "Click")
            if (operation.checkedRadioButtonId == R.id.div && b.text.toString().toFloatOrNull() ?: 0f == 0f) {
                ZeroDialog().show(childFragmentManager, null)
            } else {
                viewModel.calc(
                    a.text.toString().toFloatOrNull() ?: 0f,
                    b.text.toString().toFloatOrNull() ?: 0f,
                    operation.checkedRadioButtonId
                )
            }
        }

        constant.setOnClickListener {
            requireFragmentManager().commit {
                replace(android.R.id.content, ConstantFragment())
                addToBackStack(null)
            }
        }

        ans.setOnClickListener {
            b.setText(viewModel.ans)
        }

        share.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
                .setType("text/plain")
                .putExtra(
                    Intent.EXTRA_TEXT,
                    getString(R.string.share_text, result.text.toString())
                )
            startActivity(Intent.createChooser(intent, getString(R.string.share)))
        }

        (requireActivity() as AppCompatActivity).supportActionBar?.addOnMenuVisibilityListener {
            Log.d(TAG, "Menu: $it")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) = inflater.inflate(R.menu.menu, menu)

    override fun onOptionsItemSelected(item: MenuItem) = when(item.itemId) {
        R.id.about -> true.also {
            requireFragmentManager().commit {
                replace(android.R.id.content, AboutFragment("Honza Kaláb"))
                addToBackStack(null)
            }
        }
        else -> super.onOptionsItemSelected(item)
    }
}