package cz.gopas.calc

import android.app.Application
import androidx.annotation.IdRes
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import kotlin.concurrent.thread

class CalcViewModel(app: Application) : AndroidViewModel(app) {
    private val prefs = PreferenceManager.getDefaultSharedPreferences(app)

    private val _result = MutableLiveData<String>()
    val result: LiveData<String> = _result

    val ans: String
        get() = prefs.getString("res", "0")!!

    fun calc(a: Float, b: Float, @IdRes operation: Int) {
        thread {
            when (operation) {
                R.id.add -> a + b
                R.id.sub -> a - b
                R.id.mul -> a * b
                R.id.div -> a / b
                else -> Float.NaN
            }.toString().let { result ->
                _result.postValue(result)
                prefs.edit {
                    putString("res", result)
                }
            }
        }
    }
}