package cz.gopas.calc

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ConstantSharedViewModel: ViewModel() {
    val constant = MutableLiveData<ConstantEntity?>()
}