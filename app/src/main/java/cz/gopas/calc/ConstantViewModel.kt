package cz.gopas.calc

import android.app.Application
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.room.Room
import kotlin.concurrent.thread

class ConstantViewModel(app: Application) : AndroidViewModel(app) {
    private companion object {
        private val TAG = ConstantViewModel::class.java.simpleName
    }
    private val db = Room.databaseBuilder(
        app,
        ConstantDatabase::class.java, "constants"
    )
        //.allowMainThreadQueries()
        .build()

    val constants = db.constantDao().getAll()

    init {
        thread {
            try {
                db.constantDao().insert(
                    ConstantEntity("Pi", Math.PI.toFloat()),
                    ConstantEntity("e", Math.E.toFloat())
                )
            } catch (sqlce: SQLiteConstraintException) {
                Log.w(TAG, sqlce)
            }
        }
    }
}