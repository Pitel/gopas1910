package cz.gopas.calc

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_about.*

class AboutFragment(): Fragment(R.layout.fragment_about) {
    constructor(name: CharSequence) : this() {
        arguments = bundleOf("name" to name)
    }

    val nameArg by lazy { requireArguments().getCharSequence("name") }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        name.text = nameArg
    }
}