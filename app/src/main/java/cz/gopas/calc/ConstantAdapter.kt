package cz.gopas.calc

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

class ConstantAdapter(private val onClick: (ConstantEntity) -> Unit) :
    ListAdapter<ConstantEntity, ConstantAdapter.ConstantViewHolder>(DIFF_UTIL) {
    private companion object {
        private val DIFF_UTIL = object : DiffUtil.ItemCallback<ConstantEntity>() {
            override fun areItemsTheSame(oldItem: ConstantEntity, newItem: ConstantEntity) =
                oldItem.name == newItem.name

            override fun areContentsTheSame(oldItem: ConstantEntity, newItem: ConstantEntity) =
                oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ConstantViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(android.R.layout.simple_list_item_2, parent, false)
        )

    override fun onBindViewHolder(holder: ConstantViewHolder, position: Int) {
        val item: ConstantEntity = getItem(position)
        with(holder) {
            text1.text = item.name
            text2.text = item.value.toString()
            containerView.setOnClickListener { onClick(item) }
        }
    }

    inner class ConstantViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {
        val text1: TextView = containerView.findViewById(android.R.id.text1)
        val text2: TextView = containerView.findViewById(android.R.id.text2)
    }
}